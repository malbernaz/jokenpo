import Vue from "vue";
import App from "./App";
import store from "./lib/store";

new Vue({ el: "#app", render: h => h(App), store });
