import * as c from "./constants";
import match, { options } from "./match";

describe("match", () => {
  test("SCISSOR", () => {
    const wins = [c.PAPER, c.LIZARD];
    const losses = [c.SPOCK, c.ROCK];

    expect(wins.every(w => match(c.SCISSOR, w) === "WIN")).toBe(true);
    expect(losses.every(l => match(c.SCISSOR, l) === "LOOSE")).toBe(true);
  });

  test("PAPER", () => {
    const wins = [c.ROCK, c.SPOCK];
    const losses = [c.SCISSOR, c.LIZARD];

    expect(wins.every(w => match(c.PAPER, w) === "WIN")).toBe(true);
    expect(losses.every(l => match(c.PAPER, l) === "LOOSE")).toBe(true);
  });

  test("ROCK", () => {
    const wins = [c.LIZARD, c.SCISSOR];
    const losses = [c.PAPER, c.SPOCK];

    expect(wins.every(w => match(c.ROCK, w) === "WIN")).toBe(true);
    expect(losses.every(l => match(c.ROCK, l) === "LOOSE")).toBe(true);
  });

  test("LIZARD", () => {
    const wins = [c.SPOCK, c.PAPER];
    const losses = [c.ROCK, c.SCISSOR];

    expect(wins.every(w => match(c.LIZARD, w) === "WIN")).toBe(true);
    expect(losses.every(l => match(c.LIZARD, l) === "LOOSE")).toBe(true);
  });

  test("SPOCK", () => {
    const wins = [c.SCISSOR, c.ROCK];
    const losses = [c.LIZARD, c.PAPER];

    expect(wins.every(w => match(c.SPOCK, w) === "WIN")).toBe(true);
    expect(losses.every(l => match(c.SPOCK, l) === "LOOSE")).toBe(true);
  });

  test("match draw", () => {
    for (const opt of options) {
      expect(match(opt, opt)).toBe("DRAW");
    }
  });
});
