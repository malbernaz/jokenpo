import Vue from "vue";
import Vuex from "vuex";

import match from "./match";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    playerChoice: null,
    computerChoice: null,
    rouletteStopped: true,
    modalOpened: true,
    gameRunning: false
  },
  mutations: {
    toggleRoullete(state, stop) {
      state.rouletteStopped = stop;
    },
    setPlayerChoice(state, choice) {
      state.playerChoice = choice;
    },
    setComputerChoice(state, choice) {
      state.computerChoice = choice;
    },
    toggleModal(state, opened) {
      state.modalOpened = opened;
    },
    toggleGameRunning(state, running) {
      state.gameRunning = running;
    }
  },
  getters: {
    result(state) {
      return state.playerChoice && state.computerChoice
        ? match(state.playerChoice, state.computerChoice)
        : null;
    }
  },
  actions: {
    playGame({ commit }) {
      commit("toggleModal", false);

      setTimeout(_ => {
        commit("setPlayerChoice", null);
        commit("setComputerChoice", null);
      }, 300);
    },
    showResults({ commit }, computerChoice) {
      commit("setComputerChoice", computerChoice);
      commit("toggleModal", true);
      commit("toggleGameRunning", false);
    }
  }
});
