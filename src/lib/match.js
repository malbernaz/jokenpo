import * as c from "./constants";

const outcomes = ["WIN", "LOOSE", "DRAW"];

export const options = [c.SCISSOR, c.PAPER, c.ROCK, c.LIZARD, c.SPOCK];

export default function match(opt, opt2) {
  if (opt === opt2) {
    return outcomes[2];
  }

  switch (opt) {
    case c.SCISSOR:
      return [c.PAPER, c.LIZARD].some(o => o === opt2) ? outcomes[0] : outcomes[1];
    case c.PAPER:
      return [c.ROCK, c.SPOCK].some(o => o === opt2) ? outcomes[0] : outcomes[1];
    case c.ROCK:
      return [c.LIZARD, c.SCISSOR].some(o => o === opt2) ? outcomes[0] : outcomes[1];
    case c.LIZARD:
      return [c.SPOCK, c.PAPER].some(o => o === opt2) ? outcomes[0] : outcomes[1];
    case c.SPOCK:
      return [c.SCISSOR, c.ROCK].some(o => o === opt2) ? outcomes[0] : outcomes[1];
  }
}
