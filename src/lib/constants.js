export const SCISSOR = "✂️";
export const PAPER = "📄";
export const ROCK = "💎";
export const LIZARD = "🦎";
export const SPOCK = "🖖";
